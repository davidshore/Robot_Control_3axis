#ifndef __SPEED__H__
#define __SPEED__H__
#include "stm32f4xx_hal.h"

#define Min_Freq   5000
#define TIM_FREQ   42000000
#define STOP                                  0 // 加减速曲线状态：停止
#define ACCEL                                 1 // 加减速曲线状态：加速阶段
#define DECEL                                 2 // 加减速曲线状态：减速阶段
#define RUN                                   3 // 加减速曲线状态：匀速阶段

#define CW                          0
#define CCW                         1
typedef struct {
	uint32_t  maxfreq;		// 电机最高脉冲频率
	uint32_t  minfreq;		// 电机初始脉冲频度
	uint8_t   run_state ;  	// 电机旋转状态
	uint32_t  fre;         	// 频率计算
	uint32_t  decel_start; 	// 启动减速位置
	uint32_t  decel_val;   	// 减速阶段步数
	uint32_t  min_delay;   	// 最小脉冲周期(最大速度，即匀速段速度)
	uint32_t  accel_count; 	// 加减速阶段计数值
	uint8_t   done;			// 轴发脉冲完成标志
	int32_t   current;		// 当前位置
	uint8_t   dir;			// 脉冲计数方向
	uint32_t  count;		// 发送PWM计数
	uint8_t   full;			// 可以做整个加减速
	uint32_t  accel;		// 加速度
	float     tsqrt;		// 开方计算
	uint8_t   Deviat;		// 位置偏差或编码器方向设置错误报警
}Tspd;

extern Tspd SPD;

extern uint16_t acc[8000],dec[8000];

void TIM3_MoveAbs(uint32_t MaxFREQ,uint16_t AccTime,uint32_t Target);

#endif