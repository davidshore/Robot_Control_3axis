/**
******************************************************************************
* 文件名程: RD3M_IO.H 
* 作    者: 东莞-邓凯哥
* 功    能: IO位操作头文件
* 硬    件: RD3M-56AC
* 说明：
* 软件版本 	1.03
******************************************************************************
**/

#ifndef __RD3M_IO__H__
#define __RD3M_IO__H__
#include "stm32f4xx_hal.h"
#include "regbit.h"

#define BITBAND(addr, bitnum)    ((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(bitnum<<2)) 
#define MEM_ADDR(addr)           *((volatile unsigned long  *)(addr)) 
#define BIT_ADDR(addr, bitnum)   MEM_ADDR(BITBAND(addr, bitnum)) 

//IO口地址映射
#define GPIOA_ODR_Addr    (GPIOA_BASE+0x14) //0x4001080C  0x40020014 
#define GPIOB_ODR_Addr    (GPIOB_BASE+0x14) //0x40010C0C 
#define GPIOC_ODR_Addr    (GPIOC_BASE+0x14) //0x4001100C 
#define GPIOD_ODR_Addr    (GPIOD_BASE+0x14) //0x4001140C 
#define GPIOE_ODR_Addr    (GPIOE_BASE+0x14) //0x4001180C 
#define GPIOF_ODR_Addr    (GPIOF_BASE+0x14) //0x40011A0C    
#define GPIOG_ODR_Addr    (GPIOG_BASE+0x14) //0x40011E0C  
#define GPIOH_ODR_Addr    (GPIOH_BASE+0x14) //0x400 
#define GPIOI_ODR_Addr    (GPIOI_BASE+0x14) //0x400   



//IO模式定义
#define GPIOA_IDR_Addr    (GPIOA_BASE+0x10) //0x40010808 
#define GPIOB_IDR_Addr    (GPIOB_BASE+0x10) //0x40010C08 
#define GPIOC_IDR_Addr    (GPIOC_BASE+0x10) //0x40011008 
#define GPIOD_IDR_Addr    (GPIOD_BASE+0x10) //0x40011408 
#define GPIOE_IDR_Addr    (GPIOE_BASE+0x10) //0x40011808 
#define GPIOF_IDR_Addr    (GPIOF_BASE+0x10) //0x40011A08 
#define GPIOG_IDR_Addr    (GPIOG_BASE+0x10) //0x40011E08
#define GPIOH_IDR_Addr    (GPIOH_BASE+0x10) //
#define GPIOI_IDR_Addr    (GPIOI_BASE+0x10) //

//IO口操作,设置IO口!
#define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)  //输出 
#define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n)  //输入 

#define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)  //输出 
#define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n)  //输入 

#define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)  //输出 
#define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)  //输入 

#define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  //输出 
#define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)  //输入 

#define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  //输出 
#define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)  //输入

#define PFout(n)   BIT_ADDR(GPIOF_ODR_Addr,n)  //输出 
#define PFin(n)    BIT_ADDR(GPIOF_IDR_Addr,n)  //输入

#define PGout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //输出 
#define PGin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //输入

#define PHout(n)   BIT_ADDR(GPIOH_ODR_Addr,n)  //输出 
#define PHin(n)    BIT_ADDR(GPIOH_IDR_Addr,n)  //输入

#define PIout(n)   BIT_ADDR(GPIOI_ODR_Addr,n)  //输出 
#define PIin(n)    BIT_ADDR(GPIOI_IDR_Addr,n)  //输入


//硬件RD3M_56AC对应输入X01--X32定义
#define X01   PFin(5)
#define X02   PFin(4)
#define X03   PFin(3)
#define X04   PFin(2)		 
#define X05   PFin(1)
#define X06   PFin(0)
#define X07   PCin(15)
#define X08   PCin(14)
#define X09   PCin(13)
#define X10   PEin(6)	
#define X11   PEin(5)
#define X12   PEin(4)
#define X13   PEin(3)
#define X14   PEin(2)		 
#define X15   PEin(1)
#define X16   PEin(0)
#define X17   PBin(9)
#define X18   PBin(8)
#define X19   PBin(7)
#define X20   PBin(6)
#define X21   PGin(15)
#define X22   PGin(14)	
#define X23   PGin(13)
#define X24   PGin(12)
#define X25   PGin(11)
#define X26   PGin(10)		 
#define X27   PGin(9)
#define X28   PDin(3)
#define X29   PDin(2)
#define X30   PDin(1)
#define X31   PDin(0)
#define X32   PCin(12)

//硬件RD3M_56AC对应输出Y01--Y24定义

#define Y01 PCout(5)
#define Y02 PBout(0)
#define Y03 PBout(1)
#define Y04 PFout(11) 
#define Y05 PFout(12) 
#define Y06 PFout(13) 
#define Y07 PFout(14) 
#define Y08 PFout(15) 
#define Y09 PGout(0) 
#define Y10 PGout(1) 
#define Y11 PEout(7) 
#define Y12 PEout(8) 
#define Y13 PEout(9) 
#define Y14 PEout(10) 
#define Y15 PEout(11) 
#define Y16 PEout(12) 
#define Y17 PEout(13) 
#define Y18 PEout(14) 
#define Y19 PEout(15) 
#define Y20 PBout(10) 
#define Y21 PBout(11) 
#define Y22 PDout(8) 
#define Y23 PDout(9) 
#define Y24 PDout(10) 

//伺服使能输出IO定义
#define X_SON PCout(3) 
#define Y_SON PCout(2) 
#define Z_SON PCout(1) 

//伺服报警输入IO定义
#define X_ALM   PFin(10)
#define Y_ALM   PFin(9)
#define Z_ALM   PFin(8)
/*****************************************************************************
**函数信息 ：void IO_Input_Refresh()            
**功能描述 ：输入IO刷新状态
**输入参数 ：无
**输出参数 ：无
**说明:
*****************************************************************************/
void IO_Input_Refresh()
{
	rX[0].BIT.BIT0=X01;
	rX[0].BIT.BIT1=X02;
	rX[0].BIT.BIT2=X03;
	rX[0].BIT.BIT3=X04;
	rX[0].BIT.BIT4=X05;
	rX[0].BIT.BIT5=X06;
	rX[0].BIT.BIT6=X07;
	rX[0].BIT.BIT7=X08;
	rX[1].BIT.BIT0=X09;
	rX[1].BIT.BIT1=X10;
	rX[1].BIT.BIT2=X11;
	rX[1].BIT.BIT3=X12;
	rX[1].BIT.BIT4=X13;
	rX[1].BIT.BIT5=X14;
	rX[1].BIT.BIT6=X15;
	rX[1].BIT.BIT7=X16;
	rX[2].BIT.BIT0=X17;
	rX[2].BIT.BIT1=X18;
	rX[2].BIT.BIT2=X19;
	rX[2].BIT.BIT3=X20;
	rX[2].BIT.BIT4=X21;
	rX[2].BIT.BIT5=X22;
	rX[2].BIT.BIT6=X23;
	rX[2].BIT.BIT7=X24;
	rX[3].BIT.BIT0=X25;
	rX[3].BIT.BIT1=X26;
	rX[3].BIT.BIT2=X27;
	rX[3].BIT.BIT3=X28;
	rX[3].BIT.BIT4=X29;
	rX[3].BIT.BIT5=X30;
	rX[3].BIT.BIT6=X31;
	rX[3].BIT.BIT7=X32;
	
	
	rX[5].BIT.BIT5=X_ALM;
	rX[5].BIT.BIT6=Y_ALM;
	rX[5].BIT.BIT7=Z_ALM;
}
/*****************************************************************************
**函数信息 ：void IO_Output_Refresh()             
**功能描述 ：输出IO刷新状态
**输入参数 ：无
**输出参数 ：无
**说明:
*****************************************************************************/
void IO_Output_Refresh()
{
	Y01=rY[0].BIT.BIT0;  
	Y02=rY[0].BIT.BIT1;
	Y03=rY[0].BIT.BIT2;
	Y04=rY[0].BIT.BIT3;
	Y05=rY[0].BIT.BIT4;
	Y06=rY[0].BIT.BIT5;
	Y07=rY[0].BIT.BIT6;
	Y08=rY[0].BIT.BIT7;
	Y09=rY[1].BIT.BIT0; 
	Y10=rY[1].BIT.BIT1;
	Y11=rY[1].BIT.BIT2;
	Y12=rY[1].BIT.BIT3;
	Y13=rY[1].BIT.BIT4;
	Y14=rY[1].BIT.BIT5;
	Y15=rY[1].BIT.BIT6;
	Y16=rY[1].BIT.BIT7;
	Y17=rY[2].BIT.BIT0;  
	Y18=rY[2].BIT.BIT1;
	Y19=rY[2].BIT.BIT2;
	Y20=rY[2].BIT.BIT3;
	Y21=rY[2].BIT.BIT4;
	Y22=rY[2].BIT.BIT5;
	Y23=rY[2].BIT.BIT6;
	Y24=rY[2].BIT.BIT7;
}
#endif
